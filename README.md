# WS2812_LED

#### 介绍
使用STM32F103的DMA+SPI去驱动WS2812-LED，使用的是HAL库函数。

#### 教程查看
教程地址：  https://blog.csdn.net/xia233233/article/details/129810137?spm=1001.2014.3001.5502 
#### 安装教程

1.  克隆下载项目： https://gitee.com/xia-wang-666/ws2812_-led.git
2.  得到源文件，里面有ws2812.h和ws2812.c文件，把文件放在你的项目的头文件和源文件目录里即可
3.  完成

#### 使用说明

1.  首先需要配置你的芯片的SPI和DMA，根据ws2812的协议要求，SPI的速度在5.52——9.41MHz范围都能使用
2.  配置以STM32CUBEMX为例：
    - ①创建项目，配置好基本设置，找到SPI配置
    - ②SPI模式：发送主机模式（主要使用SPI_MOSI端口）
    - ③SPI时钟参数（CLOCK Parameters）:Baud Rate:5.52——9.41MHz范围内,Clock Phase: 2 Edge
    - ④点到DMA配置：添加DMA发送，使用默认配置（从memory取数据以Byte方式发送）
    - 初始化完成
3.  在主函数中使用
```c
#include "main.h"
#include "dma.h"
#include "spi.h"
#include "gpio.h"
#include "ws2812.h"
/* Initialize all configured peripherals */
MX_GPIO_Init();

/* USER CODE BEGIN 2 */
MX_DMA_Init();
MX_SPI1_Init();

/* USER CODE END 2 */
/* USER CODE BEGIN WHILE */
while (1)
{
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    RGB_RED(5);
	RGB_RST();
	HAL_Delay(1000);

	RGB_GREEN(4);
	RGB_RST();
	HAL_Delay(1000);

	RGB_BLUE(3);
	RGB_RST();
	HAL_Delay(1000);
}
/* USER CODE END 3 */
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


